#!/bin/sh

VIDEO_INPUT="input.mp4"
AUDIO_INPUT="input.mp3"
VIDEO_OUTPUT="output.mp4"
IMAGE_INPUT="logo.png"
SLIDESHOW_DIR=slideshow
# round up the total audio duration
AUDIODURATION=$(echo "$(soxi -D ${AUDIO_INPUT})+1" | bc | cut -d. -f1)
# add the slideshow to the audio
ffmpeg -framerate 1/60 -loop 1 -pattern_type glob -i "${SLIDESHOW_DIR}/*.jpg" -i ${AUDIO_INPUT} -vol 600 -c:v libx264 -preset veryfast -tune stillimage -crf 23 -c:a libmp3lame -b:a 192k -vf scale=-1:720 -r 25 -t ${AUDIODURATION} ${VIDEO_INPUT}
# add -16 overlay logo
ffmpeg -i ${VIDEO_INPUT} -i ${IMAGE_INPUT} \
-filter_complex "[0:v][1:v] overlay=25:25:enable='between(t,0,90)'[tmp]; \
[tmp][1] overlay=25:25:enable='between(t,358,386)'" \
-preset veryfast -tune stillimage -crf 23 -pix_fmt yuv420p -c:a copy \
${VIDEO_OUTPUT}
